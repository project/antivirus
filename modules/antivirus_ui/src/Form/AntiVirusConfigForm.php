<?php

namespace Drupal\antivirus_ui\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the global antivirus settings.
 */
class AntiVirusConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    protected EntityTypeManagerInterface $etm,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'antivirus.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'antivirus.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable antivirus scanning'),
      '#config_target' => 'antivirus.settings:enabled',
    ];
    $form['require_scan'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('File must be scanned'),
      '#config_target' => 'antivirus.settings:require_scan',
    ];
    $form['require_clean'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('File must not be infected'),
      '#config_target' => 'antivirus.settings:require_clean',
    ];

    if (!$this->hasScanners()) {
      $this
        ->messenger()
        ->addWarning($this->t('Add one or more scanners to enable scanning.'));

      foreach (['enabled', 'require_scan', 'require_clean'] as $field) {
        $form[$field] +=  [
          '#disabled' => TRUE,
          '#value' => FALSE,
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Check whether any scanners have been defined.
   *
   * @return bool
   *   TRUE if there are scanner entities.
   */
  protected function hasScanners() : bool {
    return $this
      ->etm
      ->getStorage('antivirus_scanner')
      ->getQuery()
      ->count()
      ->execute() > 0;
  }

}
